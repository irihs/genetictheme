<?php
$itemType = get_db()->getTable('ItemType')->find($item->item_type_id);
$isGeneticTableItem = $itemType->name == "Genetic Table";
$isFolio = $itemType->name == "Folio";

if ($isGeneticTableItem || $isFolio)
{
	echo head(array('bodyclass' => 'items show'));
}
else
{
	echo head(array('title' => metadata('item', array('Dublin Core', 'Title')),'bodyclass' => 'items show'));
}
?>
<style>
.gtMenu {border: 1px solid #ccc; display: inline; position: static; padding: 3px}
</style>
<div>
	<?php
	if (!$isFolio)
	{
		?><h1 style="text-transform: none"><i><?php echo metadata('item', array('Dublin Core','Title')); ?></i></h1><?php
		fire_plugin_hook('public_items_show', array('view' => $this, 'item' => $item));?>
		<?php 
	}
	else //if ($isFolio)
		fire_plugin_hook('public_items_show', array('view' => $this, 'item' => $item));
	?>
	
	<br/>
	<button id="toggleMD" onClick="doToggleMD();"></button>
	<div id="metadataDiv" style="display: none">
	    <!-- Items metadata -->
	    <div id="item-metadata">
	        <?php echo all_element_texts('item', array('show_element_sets' => array('Dublin Core')));?>
	    </div>
	
	    <h3><?php echo __('Files'); ?></h3>
	    <div id="item-images">
	         <?php echo files_for_item(); ?>
	    </div>
	
	   <?php if(metadata('item','Collection Name')): ?>
	      <div id="collection" class="element">
	        <h3><?php echo __('Collection'); ?></h3>
	        <div class="element-text"><?php echo link_to_collection_for_item(); ?></div>
	      </div>
	   <?php endif; ?>
	
	     <!-- The following prints a list of all tags associated with the item -->
	    <?php if (metadata('item','has tags')): ?>
	    <div id="item-tags" class="element">
	        <h3><?php echo __('Tags'); ?></h3>
	        <div class="element-text"><?php echo tag_string('item'); ?></div>
	    </div>
	    <?php endif;?>
	
	    <!-- The following prints a citation for this item. -->
	    <div id="item-citation" class="element">
	        <h3><?php echo __('Citation'); ?></h3>
	        <div class="element-text"><?php echo metadata('item','citation',array('no_escape'=>true)); ?></div>
	    </div>
	</div>
	<script>
	var isMDToggled = false;
	function getToggleMDLabel() {return "<?php echo __("Meta-donn&eacute;es"); ?> "+(!isMDToggled ? "&#9662;" : "&#9652;"); }
	document.getElementById("toggleMD").innerHTML = getToggleMDLabel();
	function doToggleMD()
	{
		if (isMDToggled)
			document.getElementById("metadataDiv").style.display = "none";
		else document.getElementById("metadataDiv").style.display = "block";
		isMDToggled = !isMDToggled;
		document.getElementById("toggleMD").innerHTML = getToggleMDLabel();
	}
	</script>
    
    <ul class="item-pagination navigation">
        <li id="previous-item" class="previous"><?php echo link_to_previous_item_show(); ?></li>
        <li id="next-item" class="next"><?php echo link_to_next_item_show(); ?></li>
    </ul>

</div> <!-- End of Primary. -->

 <?php echo foot(); ?>
