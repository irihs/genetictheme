<?php echo head(array('bodyid'=>'home', 'bodyclass' =>'')); ?>
<div id="primary" style="width: 100%; text-align: center">
    <?php if ($homepageText = get_theme_option('Homepage Text')): ?>
    <p><?php echo $homepageText; ?></p>
    <?php endif; ?>
    <?php if (get_theme_option('Display Featured Item') == 1): ?>
    <!-- Featured Item -->
    <div id="featured-item" class="featured">
        <?php 
        	//echo random_featured_items(1);
        	$items = get_records('Item', array('featured' => 1), 0);
			foreach ($items as $item)
			{
				//echo link_to($item, null, item_image('original', array(), 0, $item));
				//echo "<h3>".link_to($item, null, metadata($item, array('Dublin Core', 'Title')))."</h3><br/>";
				set_current_record('item', $item);
				fire_plugin_hook('public_items_browse_each', array('view' => $this, 'item' => $item));
			}
        ?>
    </div><!--end featured-item-->
    <?php endif; ?>
</div><!-- end primary -->

<?php echo foot(); ?>
